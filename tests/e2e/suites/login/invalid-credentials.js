const { By, until } = require('selenium-webdriver');
const errorHandler = require('../../utils/error-handler');
const logger = require('../../utils/logger');

const validCredentials = (driver, baseUrl, testName, sleep = 0) => {
    const loginUrl = `${baseUrl}/login`;
    const user = { username: 'unknown', password: 'wrongpassword' };

    driver
        .get(loginUrl)
        .then(() => logger.info('-----------------------------------------------------------------'))
        .then(() => logger.debug(`Starting test: ${testName}`))
        .catch(errorHandler.logAndTerminate);

    driver.sleep(sleep);

    driver
        .findElement(By.css('.component--login-form .field-name-username'))
        .sendKeys(user.username)
        .then(() => logger.info(`Username set to "${user.username}"`))
        .catch(errorHandler.logAndTerminate);

    driver.sleep(sleep);

    driver
        .findElement(By.css('.component--login-form .field-name-password'))
        .sendKeys(user.password)
        .then(() => logger.info(`Password set to "${user.password}"`))
        .catch(errorHandler.logAndTerminate);

    driver.sleep(sleep);

    driver
        .findElement(By.css('.component--login-form .field-type-submit button'))
        .click()
        .then(() => logger.info('Form submitted'))
        .catch(errorHandler.logAndTerminate);

    driver.sleep(sleep);

    driver
        .wait(until.elementLocated(By.css('.component--login-form .alert-danger')), 5000)
        .then(() => logger.info('Invalid credentials error message displayed'))
        .then(() => logger.success('SUCCESS'))
        .catch(errorHandler.logAndTerminate);
};

module.exports = validCredentials;
