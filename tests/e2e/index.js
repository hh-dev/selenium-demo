const _ = require('lodash');
const SeleniumWebdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromeDriverPath = require('chromedriver').path;
const validLoginCredentialsTest = require('./suites/login/valid-credentials');
const invalidLoginCredentialsTest = require('./suites/login/invalid-credentials');
const errorHandler = require('./utils/error-handler');

// Set up browser drivers
const service = new chrome.ServiceBuilder(chromeDriverPath).build();
chrome.setDefaultService(service);

const browsers = {
    chrome: SeleniumWebdriver.Capabilities.chrome
};

const driver = new SeleniumWebdriver.Builder()
    .withCapabilities(browsers.chrome())
    .build();

// Load test suites
const testSuites = {
    login: {
        validCredentials: validLoginCredentialsTest,
        invalidCredentials: invalidLoginCredentialsTest
    }
};

// Run tests
_.forEach(testSuites, (suite, suiteName) => {
    _.forEach(suite, (test, testName) => {
        const baseUrl = 'http://localhost:3000';
        const testInfo = { suiteName, testName };
        const sleep = 500;
        test(driver, baseUrl, testInfo, sleep);
    });
});

// Handle printing test status reporting before exiting application
process.on('beforeExit', (errorCode) => errorHandler.onProcessBeforeExit(errorCode, driver));
process.on('uncaughtException', (error) => errorHandler.logAndTerminate(error));
