# T4G Selenium Demo

## Installing dependencies
* Ensure your OS has Node v10.x and npm 6.x installed

## Running E2E tests
Before running tests, an instance of the application must be launched:
* `npm start`

Once started, run E2E tests
* `npm test`
