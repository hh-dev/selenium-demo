import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import AuthLogin from './components/auth/login';
import NavMenu from './components/common/nav-menu';
import Dashboard from './components/dashboard';
import './theme/scss/bootstrap.scss';
import './theme/scss/app.scss';

const history = createHistory();

const Root = (
    <Router history={history}>
        <div className="app">
            <NavMenu />
            <Route exact={true} path="/" component={Dashboard} />
            <Route path="/login" component={AuthLogin} />
        </div>
    </Router>
);

render(Root, document.getElementById('root'));
