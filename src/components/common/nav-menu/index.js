import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class NavMenu extends Component {
    constructor() {
        super();

        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(eventKey) {
        switch (eventKey) {
            case 'sign-in':
                window.location.href = '/login';
                break;

            default:
                break;
        }
    }

    render() {
        return (
            <div className="component--nav-menu">
                <Navbar inverse={true}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/">Home</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav onSelect={this.handleSelect}>
                        <NavItem eventKey={'sign-in'} href="#">
                            Sign In
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}

export default NavMenu;
