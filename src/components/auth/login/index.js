import React, { Component } from 'react';
import { Grid, Row, Col, Button, Alert } from 'react-bootstrap';
import Card from '@material-ui/core/Card';

class AuthLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                username: '',
                password: ''
            },
            alert: {
                style: '',
                text: '',
                visible: false
            }
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleAlertMessageDismiss = this.handleAlertMessageDismiss.bind(this);
    }

    handleInputChange(e, type) {
        const { value } = e.target;
        const nextState = Object.assign({}, this.state);
        nextState.form[type] = value;

        this.setState(nextState);
    }

    handleFormSubmit(e) {
        e.preventDefault();

        // Ensure the submit message is not visible until the response is complete
        this.setState({ alert: { visible: false } });
        const { username, password } = this.state.form;

        if (username === 'demo' && password === 't4g1234') {
            this.handleFormSuccess();
        } else {
            this.handleFormError();
        }
    }

    handleFormSuccess() {
        const nextState = Object.assign({}, this.state);
        nextState.alert = {
            style: 'success',
            text: 'Login Successful!',
            visible: true
        };

        this.setState(nextState);
    }

    handleFormError() {
        const nextState = Object.assign({}, this.state);
        nextState.alert = {
            style: 'danger',
            text: 'Login failed!',
            visible: true
        };

        this.setState(nextState);
    }

    handleAlertMessageDismiss() {
        const nextState = Object.assign({}, this.state);
        nextState.form.username = '';
        nextState.form.password = '';
        nextState.alert.visible = false;

        this.setState(nextState);
    }

    renderEmailInput() {
        return (
            <input
                className="field field-type-text field-name-username"
                name="username"
                type="text"
                placeholder="Username"
                value={this.state.form.username}
                onChange={(e) => this.handleInputChange(e, 'username')}
            />
        );
    }

    renderPassword() {
        return (
            <input
                className="field field-type-text field-name-password"
                name="password"
                type="password"
                placeholder="Password"
                value={this.state.form.password}
                onChange={(e) => this.handleInputChange(e, 'password')}
            />
        );
    }

    render() {
        let alertMessage = null;

        if (this.state.alert.visible) {
            alertMessage = (
                <Alert bsStyle={this.state.alert.style} onDismiss={this.handleAlertMessageDismiss}>
                    <p>{this.state.alert.text}</p>
                </Alert>
            );
        }

        return (
            <Grid className="component--login-form">
                <Row>
                    <Col xs={12} smOffset={4} sm={4}>
                        <Card className="form-wrapper text-center">
                            <h2>Sign In DEMO</h2>
                            <br />
                            {alertMessage}
                            <form onSubmit={this.handleFormSubmit}>
                                {this.renderEmailInput()}
                                {this.renderPassword()}
                                <div className="field field-type-submit">
                                    <Button
                                        type="submit"
                                        bsStyle="primary"
                                    >
                                        Sign In
                                    </Button>
                                </div>
                                <div className="forgot-password">
                                    <a>Forgot your password?</a>
                                </div>
                                <div className="register">
                                    <span>Don&#39;t have an account yet?</span> <a>Join Now</a>
                                </div>
                            </form>
                        </Card>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default AuthLogin;
