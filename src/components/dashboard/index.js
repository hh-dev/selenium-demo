import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

class Dashboard extends Component {
    render() {
        return (
            <div className="dashboard">
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <h3>React demo with E2E tests</h3>
                            <p>To run end to end tests, launch a new terminal, navigate to the project&#39;s
                                root directory, and then run <code>npm test</code></p>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Dashboard;
